// $Id$

Drupal.behaviors.ufajaxAutoBehavior = function (context) {
  $('#ufajax-wrapper-title input:not(.ufajax-behavior-processed)', context)
    .addClass('ufajax-behavior-processed')
    .each(function () {
      var titleField = $(this);
      
      var ufajaxPos = titleField.position();
      var ufajaxWidth = titleField.width();
      
      Drupal.ufajaxCheckTitle = '';
      $('#ufajax-informer-title')
        .css({left: (ufajaxPos.left+ufajaxWidth+10)+'px', top: (ufajaxPos.top)+'px'})
        .show(); 
      
      titleField
        .keyup(function () {
          if(titleField.val() != Drupal.ufajaxCheckTitle) {
            clearTimeout(Drupal.ufajaxCheckTimer);
            Drupal.ufajaxCheckTimer = setTimeout(function () {ufajaxCheck(titleField)}, Drupal.settings.ufajax.delay*1000);
          
            if(!$("#ufajax-informer-title").hasClass('ufajax-check-informer-progress')) {
              $("#ufajax-informer-title")
                .removeClass('ufajax-check-informer-accepted')
                .removeClass('ufajax-check-informer-rejected');
            }
              
            $("#ufajax-message-title")
              .hide();
          }
        })
        .blur(function () {
          if(titleField.val() != Drupal.ufajaxCheckTitle) {
            ufajaxCheck(titleField);
          }
        });
    });

  $('#ufajax-wrapper-field input:not(.ufajax-behavior-processed)', context)
    .addClass('ufajax-behavior-processed')
    .each(function () {
      var titleField = $(this);
      
      var ufajaxPos = titleField.position();
      var ufajaxWidth = titleField.width();
      
      Drupal.ufajaxfieldsCheckTitle = '';
      $('#ufajax-informer-field')
        .css({left: (ufajaxPos.left+ufajaxWidth+10)+'px', top: (ufajaxPos.top)+'px'})
        .show(); 
      
      titleField
        .keyup(function () {
          if(titleField.val() != Drupal.ufajaxfieldsCheckTitle) {
            clearTimeout(Drupal.ufajaxCheckTimer);
            Drupal.ufajaxfieldsCheckTimer = setTimeout(function () {ufajaxfieldsCheck(titleField)}, Drupal.settings.ufajaxfields.delay*1000);
          
            if(!$("#ufajax-informer-field").hasClass('ufajax-check-informer-progress')) {
              $("#ufajax-informer-field")
                .removeClass('ufajax-check-informer-accepted')
                .removeClass('ufajax-check-informer-rejected');
            }
              
            $("#ufajax-message-field")
              .hide();
          }
        })
        .blur(function () {
          if(titleField.val() != Drupal.ufajaxfieldsCheckTitle) {
            ufajaxfieldsCheck(titleField);
          }
        });  
    });

  $('#edit-taxonomy-tags-1-wrapper input:not(.ufajax-behavior-processed)', context)
    .addClass('ufajax-behavior-processed')
    .each(function () {
      var colorField = $(this);
      var typeField = $('#edit-field-type-0-value-wrapper input');
      var ufajaxPos = colorField.position();
      var ufajaxWidth = colorField.width();

      Drupal.ufajaxCheckColor = '';
      $('#ufajax-informer-title')
        .css({left: (ufajaxPos.left+ufajaxWidth+10)+'px', top: (ufajaxPos.top)+'px'})
        .show(); 

      colorField
        .keyup(function () {
          if(colorField.val() != Drupal.ufajaxCheckColor) {
            clearTimeout(Drupal.ufajaxCheckTimer);
            Drupal.ufajaxCheckTimer = setTimeout(function () {ufajaxtagsCheck(colorField, typeField)}, Drupal.settings.ufajax.delay*1000);
            
            if(!$("#ufajax-informer-title").hasClass('ufajax-check-informer-progress')) {
              $("#ufajax-informer-title")
                .removeClass('ufajax-check-informer-accepted')
                .removeClass('ufajax-check-informer-rejected');
            }

            $("#ufajax-message-title")
              .hide();
          }
        })
        .blur(function () {
          if(colorField.val() != Drupal.ufajaxCheckColor) {
            ufajaxtagsCheck(colorField, typeField);
          }
        });
      }
    );

  $('#edit-field-type-0-value-wrapper input:not(.ufajax-behavior-processed)', context)
    .addClass('ufajax-behavior-processed')
    .each(function () {
      var typeField = $(this);
      var colorField = $('#edit-taxonomy-tags-1-wrapper input');
      var ufajaxPos = typeField.position();
      var ufajaxWidth = typeField.width();

      Drupal.ufajaxCheckType = '';

      typeField
        .keyup(function () {
          if(typeField.val() != Drupal.ufajaxCheckType) {
            clearTimeout(Drupal.ufajaxCheckTimer);
            Drupal.ufajaxCheckTimer = setTimeout(function () {ufajaxtagsCheck(colorField, typeField)}, Drupal.settings.ufajax.delay*1000);
            
            if(!$("#ufajax-informer-title").hasClass('ufajax-check-informer-progress')) {
              $("#ufajax-informer-title")
                .removeClass('ufajax-check-informer-accepted')
                .removeClass('ufajax-check-informer-rejected');
            }

            $("#ufajax-message-title")
              .hide();
          }
        })
        .blur(function () {
          if(typeField.val() != Drupal.ufajaxCheckType) {
            ufajaxtagsCheck(colorField, typeField);
          }
        });
      }
    );
}

function ufajaxCheck(titleField) {
  clearTimeout(Drupal.ufajaxCheckTimer);
  Drupal.ufajaxCheckTitle = titleField.val();
  
  $.ajax({
    url: Drupal.settings.ufajax.ajaxUrl,
    data: {
      title: Drupal.ufajaxCheckTitle,
      ntype: Drupal.settings.ufajax.ntype,
      nid: Drupal.settings.ufajax.nid
    },
    dataType: 'json',
    beforeSend: function() {
      $("#ufajax-informer-title")
        .removeClass('ufajax-informer-accepted')
        .removeClass('ufajax-informer-rejected')
        .addClass('ufajax-informer-progress');
    },
    success: function(ret){
      if(ret['allowed']){
        $("#ufajax-informer-title")
          .removeClass('ufajax-informer-progress')
          .addClass('ufajax-informer-accepted');
        
        titleField
          .removeClass('error');
      }
      else {
        $("#ufajax-informer-title")
          .removeClass('ufajax-informer-progress')
          .addClass('ufajax-informer-rejected');
        
        $("#ufajax-message-title")
          .addClass('ufajax-message-rejected')
          .html(ret['msg'])
          .show();
      }
    }
   });
}

function ufajaxtagsCheck(colorField, typeField) {
  clearTimeout(Drupal.ufajaxCheckTimer);
  Drupal.ufajaxtagsCheckColor = colorField.val();
  Drupal.ufajaxtagsCheckType = typeField.val();

  $.ajax({
    url: Drupal.settings.ufajaxtags.ajaxUrl,
    data: {
      color: Drupal.ufajaxtagsCheckColor,
      type: Drupal.ufajaxtagsCheckType,
      nid: Drupal.settings.ufajaxtags.nid
    },
    dataType: 'json',
    beforeSend: function() {
      $("#ufajax-informer-title")
        .removeClass('ufajax-informer-accepted')
        .removeClass('ufajax-informer-rejected')
        .addClass('ufajax-informer-progress');
    },
    success: function(ret){
      if(ret['allowed']){
        $("#ufajax-informer-title")
          .removeClass('ufajax-informer-progress')
          .addClass('ufajax-informer-accepted');
        
        $("#ufajax-message-title")
          .removeClass('ufajax-message-rejected')
          .addClass('ufajax-message-accepted')
          .html(ret['msg'])
          .show();
      }
      else {
        $("#ufajax-informer-title")
          .removeClass('ufajax-informer-progress')
          .removeClass('ufajax-informer-accepted')
          .addClass('ufajax-informer-rejected');
        
        $("#ufajax-message-title")
          .removeClass('ufajax-message-accepted')
          .addClass('ufajax-message-rejected')
          .html(ret['msg'])
          .show();
      }
    }
  });
}

function ufajaxfieldsCheck(titleField) {
  clearTimeout(Drupal.ufajaxCheckTimer);
  Drupal.ufajaxfieldsCheckTitle = titleField.val();
  
  $.ajax({
    url: Drupal.settings.ufajaxfields.ajaxUrl,
    data: {
      title: Drupal.ufajaxfieldsCheckTitle,
      node: Drupal.settings.ufajaxfields.node
    },
    dataType: 'json',
    beforeSend: function() {
      $("#ufajax-informer-field")
        .removeClass('ufajax-informer-accepted')
        .removeClass('ufajax-informer-rejected')
        .addClass('ufajax-informer-progress');
    },
    success: function(ret){
      if(ret['allowed']){
        $("#ufajax-informer-field")
          .removeClass('ufajax-informer-progress')
          .addClass('ufajax-informer-accepted');
        
        titleField
          .removeClass('error');
      }
      else {
        $("#ufajax-informer-field")
          .removeClass('ufajax-informer-progress')
          .addClass('ufajax-informer-rejected');
        
        $("#ufajax-message-field")
          .addClass('ufajax-message-rejected')
          .html(ret['msg'])
          .show();
      }
    }
   });
}
